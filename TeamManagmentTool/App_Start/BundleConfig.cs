﻿using System.Web;
using System.Web.Optimization;

namespace TeamManagmentTool
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.1.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapjs").Include(
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/bootstrap-datetimepicker.min.js"));

            bundles.Add(new StyleBundle("~/bundles/charts").Include(
                        "~/Scripts/highstock.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrapcss").Include(
                        "~/Content/bootstrap.min.css",
                        "~/Content/bootstrap-datetimepicker.min.css"));

            bundles.Add(new StyleBundle("~/bundles/commonStyles").Include(
                        "~/Content/Site.css"));
        }
    }
}