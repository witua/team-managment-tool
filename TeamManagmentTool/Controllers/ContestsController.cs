﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMT.Core.Models.Contests;
using TMT.Core.Models;
using TMT.Core.DataManagers;

namespace TeamManagmentTool.Controllers
{
    [Authorize]
    public class ContestsController : BaseController
    {
        public ContestsController(IDataManager dataManager)
        {
            base.dataManager = dataManager;
        }

        public ActionResult Index()
        {
            IEnumerable<Contest> contests = DataManager.GetContests();
            return View(contests);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.SubmitAction = "Create";
            ViewBag.ActionTitle = "Create contest";
            return View(new ContestCreateModel(UserId));
        }

        [HttpPost]
        public ActionResult Create(ContestCreateModel model)
        {
            Contest contest = model.ToContest();
            DataManager.CreateContest(contest);
            for (int i = 0; i < model.ProblemsCount; ++i )
            {
                DataManager.CreateProblem(new Problem
                {
                    ContestId = contest.Id,
                    Solved = false,
                    Comments = "",
                    ProblemId = i,
                    Date = DateTime.Now
                });
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int contestId)
        {
            ViewBag.SubmitAction = "Edit";
            ViewBag.ActionTitle = "Edit contest";
            return View("Create", new ContestCreateModel(DataManager.GetContests().First(c => c.Id == contestId)));
        }

        [HttpPost]
        public ActionResult Edit(ContestCreateModel model)
        {
            Contest contest = model.ToContest();
            DataManager.UpdateContest(contest);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int contestId)
        {
            DataManager.DeleteContestById(contestId);
            return RedirectToAction("Index");
        }
    }
}
