﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMT.Core.DataManagers;
using TMT.Core.Models;
using TMT.Core.Models.Problems;

namespace TeamManagmentTool.Controllers
{
    [Authorize]
    public class ProblemsController : BaseController
    {
        public ProblemsController(IDataManager dataManager)
            : base()
        {
            base.dataManager = dataManager;
        }

        public ActionResult Index(bool? solved = null)
        {
            IEnumerable<Problem> problems = DataManager.GetProblems().Where(p => solved == null || (bool)p.Solved == solved).ToList();
            ViewBag.FilterBySolved = solved;
            return View(problems);
        }
        public ActionResult Solve(int problemId)
        {
            DataManager.SolveProblem(problemId);
            return RedirectToAction("Index");
        }
        public ActionResult Unsolve(int problemId)
        {
            DataManager.UnsolveProblem(problemId);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int problemId)
        {
            Problem problem = DataManager.GetProblems().Where(p => p.Id == problemId).FirstOrDefault();
            if (problem != null)
            {
                ProblemEditModel problemEditModel = new ProblemEditModel(problem);
                return View(problemEditModel);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(ProblemEditModel problemEditModel)
        {
            Problem problem = problemEditModel.ToProblem();
            DataManager.UpdateProblem(problem);
            return RedirectToAction("Index");
        }
    }
}
