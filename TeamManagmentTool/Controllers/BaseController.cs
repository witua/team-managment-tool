﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using TMT.Core.DataManagers;
using TeamManagmentTool.Models;
using TMT.Core.Models;

namespace TeamManagmentTool.Controllers
{
    public class BaseController : Controller
    {
        public int UserId 
        { 
            get {
                if (User != null)
                {
                    string userName = User.Identity.Name;
                    Models.UsersContext usersContext = new Models.UsersContext();
                    int userId = usersContext.UserProfiles.First(u => u.UserName == userName).UserId;
                    return userId;
                }
                return -1;
            }
        }

        protected IDataManager dataManager;

        public IDataManager DataManager
        {
            get
            {
                dataManager.SetUserId(UserId);
                return dataManager;
            }
        }
    }
}
