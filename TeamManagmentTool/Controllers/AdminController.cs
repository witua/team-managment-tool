﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMT.Core.DataManagers;
using TMT.Core.Models;
using TMT.Core.Models.Admin;

namespace TeamManagmentTool.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : BaseController
    {
        public AdminController(IDataManager dataManager)
        {
            base.dataManager = dataManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllSolved()
        {
            IEnumerable<Problem> allProblems = DataManager.GetAllProblems().OrderBy(p => p.Date);
            List<SolvedProblem> models = new List<SolvedProblem>();
            foreach (Problem problem in allProblems)
            {
                if (problem.Solved == true)
                {
                    Contest contest = problem.Contest;
                    string userName = DataManager.GetUserById((int)contest.UserId).UserName;
                    models.Add(new SolvedProblem(userName, problem));
                }
            }
            return View("AllProblems", models);
        }

        public ActionResult BlockedProblems()
        {
            IEnumerable<Problem> allProblems = DataManager.GetAllProblems();
            List<BlockedProblem> models = new List<BlockedProblem>();
            foreach (Problem problem in allProblems)
            {
                Contest contest = problem.Contest;
                string userName = DataManager.GetUserById((int)contest.UserId).UserName;
                models.Add(new BlockedProblem(userName, problem));
            }
            return View("BlockedProblems", models);
        }

        public ActionResult BlockProblem(int problemId)
        {
            DataManager.BlockProblem(problemId);
            return RedirectToAction("BlockedProblems");
        }

        public ActionResult UnblockProblem(int problemId)
        {
            DataManager.UnblockProblem(problemId);
            return RedirectToAction("BlockedProblems");
        }

        public ActionResult ContestsInfo()
        {
            IEnumerable<Contest> allContests = DataManager.GetAllContests();
            List<ContestWithUser> models = new List<ContestWithUser>();
            foreach (Contest contest in allContests)
            {
                string userName = DataManager.GetUserById((int)contest.UserId).UserName;
                models.Add(new ContestWithUser(userName, contest));
            }
            return View("ContestsInfo", models);
        }
    }
}
