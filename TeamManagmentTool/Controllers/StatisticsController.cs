﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMT.Core.DataManagers;

namespace TeamManagmentTool.Controllers
{
    [Authorize]
    public class StatisticsController : BaseController
    {
        public StatisticsController(IDataManager dataManager)
            : base()
        {
            base.dataManager = dataManager;
        }

        public ActionResult Index()
        {
            return View(DataManager.GetContests().Where(c => c.Result != null));
        }

    }
}
