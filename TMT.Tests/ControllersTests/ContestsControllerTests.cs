﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TMT.Core.Models.Contests;
using TeamManagmentTool.Controllers;
using TMT.Core.DataManagers;
using TMT.Core.Models;

namespace TMT.Tests.ControllersTests
{
    /// <summary>
    /// Summary description for ContestsControllerTests
    /// </summary>
    [TestClass]
    public class ContestsControllerTests
    {
        [TestMethod]
        public void ContestCreatingTest()
        {
            Mock<IDataManager> dataManager = new Mock<IDataManager>();
            ContestCreateModel contest = new ContestCreateModel
            {
                Id = 47,
                Name = "Name",
                Description = "Description",
                ProblemsCount = 7,
                Result = 1,
                Date = DateTime.Now,
                UserId = 74
            };
            ContestsController target = new ContestsController(dataManager.Object);
            target.Create(contest);
            dataManager.Verify(m => m.CreateProblem(It.Is<Problem>(p => p.ContestId == contest.Id)), Times.Exactly(contest.ProblemsCount));
            dataManager.Verify(m => m.CreateContest(It.IsAny<Contest>()), Times.Once);
        }
    }
}
