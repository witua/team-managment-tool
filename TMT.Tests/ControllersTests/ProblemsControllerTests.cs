﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TMT.Core.DataManagers;
using Moq;
using TMT.Core.Models;
using TeamManagmentTool.Controllers;
using System.Web.Mvc;

namespace TMT.Tests.ControllersTests
{
    [TestClass]
    public class ProblemsControllerTests
    {
        [TestMethod]
        public void ProblemsFilteringTest()
        {
            Mock<IDataManager> dataManager = new Mock<IDataManager>();
            dataManager.Setup(m => m.GetProblems()).Returns(new List<Problem> {
                new Problem {
                    Id = 0,
                    ContestId = 0,
                    ProblemId = 0,
                    Solved = true,
                    Comments = ""
                },
                new Problem {
                    Id = 1,
                    ContestId = 0,
                    ProblemId = 0,
                    Solved = false,
                    Comments = ""
                },
                new Problem {
                    Id = 2,
                    ContestId = 0,
                    ProblemId = 0,
                    Solved = true,
                    Comments = ""
                },
                new Problem {
                    Id = 3,
                    ContestId = 0,
                    ProblemId = 0,
                    Solved = true,
                    Comments = ""
                },
            });
            ProblemsController target = new ProblemsController(dataManager.Object);

            Assert.AreEqual(((List<Problem>)(((ViewResult)target.Index()).ViewData.Model)).Count, 4);
            Assert.AreEqual(((List<Problem>)(((ViewResult)target.Index(true)).ViewData.Model)).Count, 3);
            Assert.AreEqual(((List<Problem>)(((ViewResult)target.Index(false)).ViewData.Model)).Count, 1);
            
        }
    }
}
