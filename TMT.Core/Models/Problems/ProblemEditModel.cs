﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMT.Core.Models.Problems
{
    public class ProblemEditModel
    {
        public int Id { get; set; }

        public int ContestId { get; set; }

        [DisplayName("Problem ID")]
        public int ProblemId { get; set; }

        [DisplayName("Comments")]
        public string Comments { get; set; }

        [DisplayName("Solved")]
        public bool Solved { get; set; }

        [DisplayName("Contest")]
        public string ContestName { get; set; }

        public ProblemEditModel()
        {
        }

        public ProblemEditModel(Problem problem)
        {
            Id = problem.Id;
            ContestId = problem.ContestId;
            ProblemId = problem.ProblemId;
            Comments = problem.Comments;
            Solved = problem.Solved == null ? false : (bool)problem.Solved;
            ContestName = problem.Contest.Name;
        }

        public Problem ToProblem()
        {
            return new Problem
            {
                ProblemId = ProblemId,
                ContestId = ContestId,
                Comments = Comments,
                Solved = Solved,
                Id = Id
            };
        }
    }
}
