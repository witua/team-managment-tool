﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMT.Core.Models.Admin
{
    public class ContestWithUser
    {
        public Contest Contest { get; set; }

        public string UserName { get; set; }

        public ContestWithUser()
        { }

        public ContestWithUser(string userName, Contest contest)
        {
            Contest = contest;
            UserName = userName;
        }
    }
}
