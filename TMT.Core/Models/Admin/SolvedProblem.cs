﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMT.Core.Models.Admin
{
    public class SolvedProblem
    {
        public string UserName { get; set; }

        public string ContestName { get; set; }

        public int ProblemId { get; set; }

        public Nullable<DateTime> Date { get; set; }

        public SolvedProblem()
        {
        }

        public SolvedProblem(string UserName, Problem problem)
        {
            this.UserName = UserName;
            ContestName = problem.Contest.Name;
            ProblemId = problem.ProblemId;
            Date = problem.Date;
        }
    }
}
