﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMT.Core.Models.Admin
{
    public class BlockedProblem
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string ContestName { get; set; }

        public int ProblemId { get; set; }

        public DateTime Date { get; set; }

        public Nullable<bool> Blocked { get; set; }

        public BlockedProblem()
        {
        }

        public BlockedProblem(string UserName, Problem problem)
        {
            this.UserName = UserName;
            Id = problem.Id;
            ContestName = problem.Contest.Name;
            ProblemId = problem.ProblemId;
            Date = DateTime.Now;
            Blocked = problem.Blocked;
        }
    }
}
