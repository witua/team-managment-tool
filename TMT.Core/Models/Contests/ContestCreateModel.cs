﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using TMT.Core.Models;

namespace TMT.Core.Models.Contests
{
    public class ContestCreateModel
    {
        public int Id { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Number of problems")]
        public int ProblemsCount { get; set; }

        [DisplayName("Result")]
        public int? Result { get; set; }

        [DisplayName("When")]
        public DateTime Date { get; set; }

        public int UserId { get; set; }

        public ContestCreateModel()
        {

        }

        public ContestCreateModel(int userId)
        {
            Id = 0;
            UserId = userId;
            Name = string.Empty;
            Description = string.Empty;
            ProblemsCount = 7;
            Result = null;
            Date = DateTime.Now;
        }

        public ContestCreateModel(Contest contest)
        {
            Id = contest.Id;
            Name = contest.Name;
            Description = contest.Description;
            UserId = (int)contest.UserId;
            Result = contest.Result;
            ProblemsCount = contest.Problems.Count;
            Date = (DateTime)contest.Date;
        }

        public Contest ToContest()
        {
            return new Contest()
            {
                Id = Id,
                Name = Name,
                Description = Description,
                Date = Date,
                UserId = UserId,
                Result = Result
            };
        }
    }
}