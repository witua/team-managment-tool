﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TMT.Core.DataManagers;
using TMT.Core.Models;

namespace TMT.Core.DataManagers
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class DataManager : IDataManager
    {
        private const string connectionString = "";

        private int userId = -1;

        private tmtEntities getDbContext()
        {
            return new tmtEntities();
        }

        private UsersContext getUsersContext()
        {
            return new UsersContext();
        }

        public DataManager()
        {
            using (tmtEntities db = getDbContext())
            {
                //this.userId = userId;
                /*User user = db.Users.FirstOrDefault(u => u.UserName == userName);
                if (user != null)
                {
                    userId = user.Id;
                }*/
            }
        }

        public void SetUserId(int userId)
        {
            this.userId = userId;
        }

        #region Contests

        public IEnumerable<Contest> GetContests()
        {
            using (tmtEntities db = getDbContext())
            {
                return db.Contests.Where(c => c.UserId == userId).Include(c => c.Problems).ToList();
            }
        }

        public IEnumerable<Contest> GetAllContests()
        {
            using (tmtEntities db = getDbContext())
            {
                return db.Contests.Include(c => c.Problems).ToList();
            }
        }

        public void CreateContest(Contest contest)
        {
            using (tmtEntities db = getDbContext())
            {
                db.Contests.Add(contest);
                db.SaveChanges();
            }
        }

        public void DeleteContestById(int contestId)
        {
            using (tmtEntities db = getDbContext())
            {
                db.Contests.Remove(db.Contests.First(c => c.Id == contestId));
                db.SaveChanges();
            }
        }

        public void UpdateContest(Contest contest)
        {
            using (tmtEntities db = getDbContext())
            {
                db.Contests.Attach(contest);
                var entry = db.Entry(contest);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        #endregion

        #region Problems

        public IEnumerable<Problem> GetProblems()
        {
            using (tmtEntities db = getDbContext())
            {
                return db.Problems.Where(p => p.Contest.UserId == userId).Include(p => p.Contest).ToList();
            }
        }

        public IEnumerable<Problem> GetAllProblems()
        {
            using (tmtEntities db = getDbContext())
            {
                return db.Problems.Include(p => p.Contest).ToList();
            }
        }

        public void CreateProblem(Problem problem)
        {
            using (tmtEntities db = getDbContext())
            {
                db.Problems.Add(problem);
                db.SaveChanges();
            }
        }

        public void SolveProblem(int problemId)
        {
            using (tmtEntities db = getDbContext())
            {
                Problem problem = db.Problems.First(p => p.Id == problemId);
                problem.Solved = true;
                problem.Date = DateTime.Now;
                db.SaveChanges();
            }
        }

        public void BlockProblem(int problemId)
        {
            using (tmtEntities db = getDbContext())
            {
                Problem problem = db.Problems.First(p => p.Id == problemId);
                problem.Blocked = true;
                db.SaveChanges();
            }
        }

        public void UnsolveProblem(int problemId)
        {
            using (tmtEntities db = getDbContext())
            {
                Problem problem = db.Problems.First(p => p.Id == problemId);
                problem.Solved = false;
                problem.Date = DateTime.Now;
                db.SaveChanges();
            }
        }

        public void UnblockProblem(int problemId)
        {
            using (tmtEntities db = getDbContext())
            {
                Problem problem = db.Problems.First(p => p.Id == problemId);
                problem.Blocked = false;
                db.SaveChanges();
            }
        }

        public void UpdateProblem(Problem problem)
        {
            using (tmtEntities db = getDbContext())
            {
                db.Problems.Attach(problem);
                var entry = db.Entry(problem);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        #endregion

        #region Users

        public UserProfile GetUserById(int userId)
        {
            UserProfile user = null;
            using (UsersContext db = getUsersContext())
            {
                user = db.UserProfiles.First(u => u.UserId == userId);
            }
            return user;
        }

        #endregion
    }
}