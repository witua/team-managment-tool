﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMT.Core.Models;

namespace TMT.Core.DataManagers
{
    public interface IDataManager
    {
        void SetUserId(int userId);

        #region Contests

        IEnumerable<Contest> GetContests();

        IEnumerable<Contest> GetAllContests();

        void CreateContest(Contest contest);

        void DeleteContestById(int contestId);

        void UpdateContest(Contest contest);

        #endregion

        #region Problems

        IEnumerable<Problem> GetProblems();

        IEnumerable<Problem> GetAllProblems();

        void CreateProblem(Problem problem);

        void UpdateProblem(Problem problem);

        void SolveProblem(int problemId);

        void UnsolveProblem(int problemId);

        void BlockProblem(int problemId);

        void UnblockProblem(int problemId);


        #endregion

        #region Users

        UserProfile GetUserById(int userId);

        #endregion
    }
}
