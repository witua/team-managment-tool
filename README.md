Team managment tool

description:
A web-site tool for ICPC team coach and members to manage training proccess, with possibility to add contests, solved and upsolved problems, view current statistics in charts.

user can:
-authentication
1. register/login
-contests
2. view contests
3. add contest
4. edit contest
5. delete contest
-problems
6. view problems
7. filter problems
8. solve/unsolve problem
9. edit problem
10. comment on problem
-statisticts
10. view graphical statistics on contest results